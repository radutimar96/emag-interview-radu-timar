<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;
use App\Models\UserAccessedLink;
use App\Models\User;
use Carbon\Carbon;
use DB;

class UserAccessedLinkController extends Controller
{
	public function store(Request $request){

        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'link' => 'required',
            'link_type' => ['required', Rule::in(['product', 'category', 'static-page', 'checkout', 'homepage'])],
        ]);

        if ($validator->fails()){
			$errors = collect($validator->errors());
			$error  = $errors->unique()->first();
			return response()->json(['status' => 'error','message' => $error[0]]);
		}

        try{
            $data = UserAccessedLink::create(
                [
                    'user_id' => $request->user_id,
                    'link' => $request->link,
                    'link_type' => $request->link_type,
                ]
            );
            return response()->json(['status' => 'success', 'message' => 'User accessed link saved successfully', 'data' => $request->all()]);
        }catch( \Illuminate\Database\QueryException $e) {
            return response()->json(['status' => 'error','message' => $e->getMessage()]);
        }
    }

    public function getHits(Request $request){

        $validator = Validator::make($request->all(), [
            'from' => 'required|date|date_format:d-m-Y H:i:s',
            'to' => 'required|date|date_format:d-m-Y H:i:s',
            'link' => 'required_if:type,link'
        ]);

        if ($validator->fails()){
			$errors = collect($validator->errors());
			$error  = $errors->unique()->first();
			return response()->json(['status' => 'error','message' => $error[0]]);
		}

        $from = Carbon::createFromFormat('d-m-Y H:i:s', $request->from);
        $to = Carbon::createFromFormat('d-m-Y H:i:s', $request->to);
  
        if($request->type === "link") {
            // Search by link
            $links = UserAccessedLink::select('link',DB::raw('Count(*) as count'))->where('link', $request->link)->whereBetween('created_at', [$from, $to])->groupBy('link')->get();
        } else {
            // Search by type
            $links = UserAccessedLink::select('link_type',DB::raw('Count(*) as count'))->whereBetween('created_at', [$from, $to])->groupBy('link_type')->get();
        }

        return response()->json(['status' => 'success', 'data' => $links]);
    }

    public function getTrace(\App\Models\User $user) {
        $accessedLinks = $user->accessedLinks()->orderBy('created_at', 'asc')->get(['id','link']);
        $otherUsersAccessedLinks = UserAccessedLink::selectRaw("user_id,GROUP_CONCAT(link) AS `ual_links`")->where('user_id','!=',$user->id)->having('ual_links',implode(',',$accessedLinks->pluck('link')->toArray()))->groupBy('user_id')->GET();
        return response()->json(
            [
                'status' => 'success', 
                'data' => [
                    'accessedLinks' => $accessedLinks,
                    'otherUsersAccessedLinks' => $otherUsersAccessedLinks
                ]
            ]
        );
    }
}
