# Heatmap Service

## Instructions

- Copy the .env.example and rename it to .env
- Inside the .env file change `DB_DATABASE`, `DB_USERNAME`, `DB_PASSWORD` 
- Open a console in the project folder and run `composer install`
- Run `php artisan key:generate` which will generate an unique key for the application and will store it in the .env file
- If you are on Windows you need to install WSL
    - https://docs.microsoft.com/en-us/windows/wsl/install-win10
    - after you install it you can run the command `wsl` inside the projects folder and continue
- run `./vendor/bin/sail up` . this will setup the docker container and start it
- create db and user in docker with the same name/password as mentioned in .env and make sure the user has privileges.
- open the docker cli of the project and run `php artisan migrate` this will create all the tables needed in order to run the project
<br><br>
 
# REST API

## Store link data

### Request

`POST /api/v1/heatmap` <br>
`Ex: http://localhost/api/v1/heatmap`

### Body

<table style="text-align:center">
    <tr>
        <th>Key</th>
        <th>Required</th>
        <th>Description</th>
    </tr>
    <tr>
        <td>user_id</td>
        <td>true</td>
        <td>Expects the id of the user accessing a link</td>
    </tr>
    <tr>
        <td>link</td>
        <td>true</td>
        <td>The link that the user accessed</td>
    </tr>
    <tr>
        <td>link_type</td>
        <td>true</td>
        <td>The link type. <br>
        Must be one of 'product', 'category', 'static-page', 'checkout', 'homepage'</td>
    </tr>
</table>
<br><br>

## Get hits of link in a time period

### Request

`GET /api/v1/heatmap` <br>
`Ex: http://localhost/api/v1/heatmap?from=20-10-2020&to=20-10-2020&type=link&link=http://localhost/testing`

### Query parameters
<table style="text-align:center">
    <tr>
        <th>Key</th>
        <th>Required</th>
        <th>Description</th>
    </tr>
    <tr>
        <td>from</td>
        <td>true</td>
        <td>Must be the date format <b>d-m-Y H:i:s</b></td>
    </tr>
    <tr>
        <td>to</td>
        <td>true</td>
        <td>Must be the date format <b>d-m-Y H:i:s</b></td>
    </tr>
    <tr>
        <td>type</td>
        <td>true</td>
        <td>Must be <b>link</b></td>
    </tr>
    <tr>
        <td>link</td>
        <td>true</td>
        <td></td>
    </tr>
</table>
<br><br>

## Get hits for each page type in a time interval

### Request

`GET /api/v1/heatmap` <br>
`Ex: http://localhost/api/v1/heatmap?from=20-10-2020&to=20-10-2020`

### Query parameters
<table style="text-align:center">
    <tr>
        <th>Key</th>
        <th>Required</th>
        <th>Description</th>
    </tr>
    <tr>
        <td>from</td>
        <td>true</td>
        <td>Must be the date format <b>d-m-Y H:i:s</b></td>
    </tr>
    <tr>
        <td>to</td>
        <td>true</td>
        <td>Must be the date format <b>d-m-Y H:i:s</b></td>
    </tr>
</table>
<br><br>

## Get trace of user accessed links and other users that had the same journey

### Request

`GET /api/v1/heatmap` <br>
`Ex: http://localhost/api/v1/heatmap/trace/{user_id}`

### Link parameters
<table style="text-align:center">
    <tr>
        <th>Key</th>
        <th>Required</th>
        <th>Description</th>
    </tr>
    <tr>
        <td>user_id</td>
        <td>true</td>
        <td></td>
    </tr>
</table>
